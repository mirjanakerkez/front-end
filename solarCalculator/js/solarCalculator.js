// JavaScript Document
function calculateSolar() {
    var suma = 0;
    var godPotrStr = document.getElementsByTagName("input");
    for (var i = 0; i < godPotrStr.length; i++) {
        suma += parseFloat(godPotrStr[i].value);
    }
    var dnevPotrStr = Math.round(parseFloat(suma / 365));
    //   console.log(dnevPotrStr);
    
    
    var zona = document.forms.solarForm.zone.selectedIndex;
    zona += 1;
    var suncaniSati;
    switch (zona) {
    case 1:
        suncaniSati = 6;
        break;
    case 2:
        suncaniSati = 5.5;
        break;
    case 3:
        suncaniSati = 5;
        break;
    case 4:
        suncaniSati = 4.5;
        break;
    case 5:
        suncaniSati = 4.2;
        break;
    case 6:
        suncaniSati = 3.5;
        break;
    }
    var generationNeeds = dnevPotrStr * suncaniSati;
    //     alert(generationNeeds);
    var generationNeedsWh = generationNeeds * 1000;
    //    alert(generationNeedsWh);
    var final = Math.round(generationNeedsWh * 0.25);
    //       alert(final);
   
    
    var panel = document.forms.solarForm.panel.selectedIndex;
    panel += 1;
    var vrednostpanela;
    switch (panel) {
    case 1:
        vrednostpanela = 250;
        break;
    case 2:
        vrednostpanela = 275;
        break;
    case 3:
        vrednostpanela = 260;
        break;
    };
    var racun = Math.round(final / vrednostpanela);
    //    console.log(racun);
    
    
    document.getElementById("feedback").innerHTML = "Vasa dnevna potrosnja struje je " + dnevPotrStr + " kWh. U zavisnosti od odabarane zone, broja suncanih sati i vremenskih prilika Vase potrebe za elektricnom energijom su " + final + " Wh. Na osnovu izabranog tipa panela potrebno Vam je " + racun + " ploca.";
}